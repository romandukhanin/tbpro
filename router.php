<?php

namespace App;

use App\Controllers\MainController;
use App\Controllers\IndexController;
use App\Controllers\Api\BotController;
use App\Controllers\Api\AuthController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Auth;

$router = new \Klein\Klein();

$router->respond('GET', '/', function () {
    $ic = new IndexController();
    $ic->main();
});

/* $router->respond('GET', '/dev', function () {
    $bc = new BotController();
    $bc->main();
}); */

$router->respond('POST', '/api/auth/check', function () {
    $auth = new AuthController();
    $auth->check();
});

$router->respond('GET', '/api/bot/get_updates', function () {

    $auth = new Auth();
    if (!$auth->check()) {
        $auth->responseNoAuth();
        return;
    }

    $bc = new BotController();
    $bc->getUpdates();
});

$router->respond('POST', '/api/bot/send', function () {

    $auth = new Auth();
    if (!$auth->check()) {
        return $auth->responseNoAuth();
    }

    $bc = new BotController();
    $bc->sendAllMessages();
});

$router->respond('GET', '/api/bot/groups/get', function () {
    $bc = new BotController();
    $bc->getGroups();
});

/* $router->respond('GET', '/api/rdk/main', function () {
    $rdk = new \App\Services\TelegramRdk();
    $rdk->main();
}); */

$router->respond('GET', '/api/forward/messages/get', function () {

    $auth = new Auth();
    if (!$auth->check()) {
        return $auth->responseNoAuth();
    }

    $fc = new \App\Controllers\Api\ForwardController();
    $fc->getMessages();
});

$router->respond('GET', '/api/forward/messages/go', function () {

    $auth = new Auth();
    if (!$auth->check()) {
        return $auth->responseNoAuth();
    }

    $fc = new \App\Controllers\Api\ForwardController();
    $fc->forwardAllMessages();
});



$router->dispatch();
