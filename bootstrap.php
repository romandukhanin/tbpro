<?php

namespace App;

require __DIR__ . '/vendor/autoload.php';

//$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv = \Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();