<?php

namespace App\Controllers\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Auth;

class AuthController
{
    public function check()
    {
        $auth = new Auth();
        if ($auth->check()) {
            return $auth->responseAuth();
        }
        return $auth->responseNoAuth();
    }
}
