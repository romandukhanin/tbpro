<?php

namespace App\Controllers\Api;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Services\TelegramRdk;
use App\Models\Forward;
use App\Models\Group;

class ForwardController
{
    public $token;
    public $telegram;
    public $request;


    public function __construct()
    {
        $this->token = getenv('TELEGRAM_TOKEN');
        $this->request = Request::createFromGlobals();
        $this->rdk = new TelegramRdk();
    }

    public function getMessages()
    {
        $getUpdatesOptions = [
            'token' => getenv('TELEGRAM_TOKEN')
        ];
        $response = json_decode($this->rdk->getUpdates($getUpdatesOptions), true);
        if ($response['ok']) {
            $result = $response['result'];
        }
        $messages = [];
        foreach ($result as $message) {
            $update_id = $message['update_id'];
            $message = $message['message'];
            if (
                isset($message['left_chat_member'])
                || isset($message['new_chat_member'])
                || isset($message['group_chat_created'])
            ) {
                continue;
            }

            if ($message['chat']['id'] != getenv('SOURCE_GROUP_ID')) {
                continue;
            }

            $mess['update_id'] = $update_id;
            $mess['message_id'] = $message['message_id'];
            $mess['from_chat_id'] = $message['chat']['id'];
            $mess['json'] = json_encode($message, JSON_UNESCAPED_UNICODE);

            $message_for_db = [
                'update_id' => $update_id,
                'message_id' => $message['message_id'],
                'from_chat_id' => $message['chat']['id'],
                'json' => json_encode($message, JSON_UNESCAPED_UNICODE),
            ];
            $messages[] = $message_for_db;
        }

        //print_r($messages);

        $forward = new Forward();

        foreach ($messages as $message) {
            //print_r($message);

            if ($forward->isMessageInTable($message['message_id'])) {
                continue;
            }

            $message['is_forwarded'] = 0;
            $forward->insertMessageToTable($message);
        }
    }

    public function forwardAllMessages()
    {
        $forward = new Forward();
        $messages_ids = $forward->getUnfowarded();

        $group = new Group();
        $groups = $group->getGroups();

        foreach ($groups as $title => $chat_id) {
            if ($chat_id == getenv('SOURCE_GROUP_ID')) {
                continue;
            }
            foreach ($messages_ids as $message_id) {
                $forwardMessageOptions = [
                    'chat_id' => $chat_id,
                    'from_chat_id' => getenv('SOURCE_GROUP_ID'),
                    'message_id' => $message_id,
                    'token' => getenv('TELEGRAM_TOKEN')
                ];
                echo $this->rdk->forwardMessage($forwardMessageOptions);
                $forward->updateIsForwarded($message_id, 1);
            }
        }

        //print_r($messages_ids);
    }
}
