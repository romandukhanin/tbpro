<?php

namespace App\Controllers\Api;

use Telegram\Bot\Api;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Group;
use App\Services\TelegramRdk;

class BotController
{
    public $token;
    public $telegram;
    public $request;


    public function __construct()
    {
        $this->token = getenv('TELEGRAM_TOKEN');
        $this->telegram = new Api($this->token);
        $this->request = Request::createFromGlobals();
    }

    public function main()
    {
        //$this->getInfo();
    }

    public function getUpdates()
    {
        $group = new Group();
        $rdk = new TelegramRdk();
        $bot_id = explode(':', getenv('TELEGRAM_TOKEN'))[0];

        $getUpdatesOptions = [
            'token' => getenv('TELEGRAM_TOKEN')
        ];
        $updates = json_decode($rdk->getUpdates($getUpdatesOptions), true);
        if ($updates['ok']) {
            $result = $updates['result'];
        }
        $data = [];
        foreach ($result as $message) {
            $update_id = $message['update_id'];
            $message = $message['message'];
            if (!(isset($message['left_chat_member'])
                || isset($message['new_chat_member']))) {
                continue;
            }

            $bid = $message['left_chat_member']['id'] ?? $message['new_chat_member']['id'];

            if ($bid != $bot_id) {
                continue;
            }

            $group_data = [
                'chat_id' => $message['chat']['id'],
                'title' => $message['chat']['title'],
                'username' => 'no name in message',
                'date' => $message['date'],
                'status' => isset($message['new_chat_member']) ? 'member' : 'left'
            ];

            if (!$group->isChatIdInTable($group_data['chat_id'])) {
                $group->insertGroupToTable($group_data);
            } else {
                $group->updateStatus($group_data['chat_id'], $group_data['status']);
            }

            $data[] = $group_data;
        }
        //echo json_encode($data, JSON_UNESCAPED_UNICODE);

        /* $response = new Response(
            json_encode($updates, JSON_UNESCAPED_UNICODE),
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
        $response->send(); */
    }

    public function getInfo()
    {
        $response = $this->telegram->getMe();
        $botId = $response->getId();
        $firstName = $response->getFirstName();
        $username = $response->getUsername();
        echo $username;
        //print_r($response);
    }

    public function sendAllMessages()
    {
        $data = json_decode($this->request->getContent(), JSON_OBJECT_AS_ARRAY);
        $message = $data['message'];
        $groups = $data['groups'];
        foreach ($groups as $group) {
            $group = $this->groupNameFormat($group);
            $id = $this->sendMessage($message, $group);
        }
    }

    public function sendMessage($message, $group)
    {
        $telegramResponse = $this->telegram->sendMessage([
            'chat_id' => $group,
            'text' => $message
        ]);
        $messageId = $telegramResponse->getMessageId();
        return $messageId;
    }

    public function groupNameFormat($group_name)
    {
        //return '@' . $group_name;
        return $group_name;
    }

    public function getGroups()
    {
        $g = new Group();
        $groups = $g->getGroups();
        $export = [];
        foreach ($groups as $label => $value) {
            $obj = (object)[];
            $obj->value = $value;
            $obj->label = $label;
            $export[] = $obj;
        }
        $response = new Response(
            json_encode($export, JSON_UNESCAPED_UNICODE),
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
        $response->send();
    }
}
