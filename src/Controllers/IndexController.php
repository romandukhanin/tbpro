<?php

namespace App\Controllers;

use App\Views\Render;
use Symfony\Component\HttpFoundation\Request;

class IndexController
{
    public function main()
    {
        $render = new Render();
        $request = Request::createFromGlobals();
        $name = getenv('APP_NAME');
        //$name = 'Telegram PizzaBot';
        //print_r($request->headers);
        $render->render('index', ['name' => $name]);
        //echo 'MainController';
    }
}
