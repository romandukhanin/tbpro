<?php

namespace App\Controllers;

use App\Views\Render;

class MainController
{
    public function main($name)
    {
        $render = new Render();
        $render->render('test',['name' => $name]);
        //echo 'MainController';
    }
}