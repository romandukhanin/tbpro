<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <title></title>
    <meta name="description" content="">
    <link href='https://code.cdn.mozilla.net/fonts/fira.css' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="./css/element.ui.css" />
    <link rel="stylesheet" type="text/css" href="./css/main.css" />
    <script src="./js/vue.v2.6.12.js"></script>
    <script src="./js/element-ui.v2.15.1.js"></script>
    <script src="./js/element-ui.russificator.js"></script>
    <script src="./js/axios.min.js"></script>
</head>

<body>
    <div id="app" class="rd-container">
        <h1><?= $data['name'] ?></h1>
        <el-form ref="form" :model="auth" label-position="top" v-if="!is_auth">
            <el-form-item :label="auth_mess">
                <el-input placeholder="Введите пароль" v-model="pass"></el-input>
            </el-form-item>
            <el-form-item>
                <el-button type="primary" @click="makeAuth">Авторизоваться</el-button>
            </el-form-item>
        </el-form>
        <el-select v-model="groups_checked" multiple placeholder="Выберите">
            <el-option v-for="group in groups" :key="group.value" :value="group.value" :label="group.label">
            </el-option>
        </el-select>
        <el-form ref="form" :model="form" label-position="top">
            <el-form-item label="Введите сообщение:">
                <el-input type="textarea" v-model="form.message"></el-input>
            </el-form-item>
            <el-form-item>
                <el-button type="primary" @click="send">Отправить</el-button>
            </el-form-item>
        </el-form>
    </div>
</body>
<script>
    ELEMENT.locale(ELEMENT.lang.ruRU);
    new Vue({
        el: '#app',
        data: function() {
            return {
                form: {
                    message: '',
                },
                groups: [],
                groups_checked: [],
                url_base: '',
                is_auth: false,
                pass: '',
                auth: {},
                auth_mess: 'Вы не авторизованы, введите пароль:'
            }
        },
        methods: {
            makeAuth(context = 'normal') {
                that = this;
                if (!this.pass) {
                    this.getPass();
                }
                axios.post(this.url_base + '/api/auth/check', {
                        'message': 'Auth Check'
                    }, {
                        headers: {
                            'pass-token': this.pass
                        }
                    })
                    .then(function(response) {
                        console.log(response.data);
                        /* console.log('response.data: ', response.data);
                        console.log('response.data: ' + response.data);
                        console.log('response.data: ' + JSON.stringify(response.data)); */
                        JSON.stringify()
                        if (response.data.message == 'auth') {
                            that.setPass();
                            that.is_auth = true;
                        } else {
                            if (context != 'mounted') {
                                that.auth_mess = 'Ниправельна! Введите другой пароль.'
                            }
                        }
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            setPass() {
                localStorage.pass = this.pass;
            },
            getPass() {
                if (localStorage.pass) {
                    this.pass = localStorage.pass;
                }
                return this.pass;
            },
            extractFrom(arr) {
                result = [];
                arr.forEach((item) => {
                    result.push(item.value);
                });
                return result;
            },
            send() {
                that = this;
                //console.log('this.groups: ' + this.groups);
                //console.log('this.getPass(): ' + this.getPass());

                axios.post(this.url_base + '/api/bot/send', {
                        'message': this.form.message,
                        'groups': this.groups_checked
                    }, {
                        headers: {
                            'pass-token': this.getPass()
                        }
                    })
                    .then(function(response) {
                        console.log('response.data.message: ' + response.data.message);
                        console.log('response.data: ' + response.data);
                    })
                    .catch(function(error) {
                        console.log(error);
                    });
            },
            getGroups: function() {
                that = this;
                axios
                    .get(this.url_base + '/api/bot/groups/get')
                    .then(function(response) {
                        that.groups = response.data;
                        that.groups_checked = that.extractFrom(response.data);
                        //console.log(that.groups);
                    })
                    .catch(error => console.log(error));
            },
        },
        mounted() {
            this.getGroups();
            //this.getPass();
            this.makeAuth('mounted');
        },
    })
</script>

</html>