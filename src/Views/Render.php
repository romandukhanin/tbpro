<?php

namespace App\Views;

class Render
{
    public function render($view, $data = [])
    {
        $view = 'views/'.$view.'.view.php';
        ob_start();
        include($view);
        echo ob_get_clean();
    }
}
