<?php

namespace App\Models;

class Forward
{

    public $db;

    public function __construct()
    {
        $base = '../db/sqlitepizzabot.db';
        $this->db = new \SQLite3($base);
        $query = 'CREATE TABLE IF NOT EXISTS messages_to_forward (id INTEGER PRIMARY KEY, update_id STRING, message_id STRING UNIQUE, from_chat_id STRING, json STRING, is_forwarded INTEGER)';
        $this->db->exec($query);
    }

    public function insertMessageToTable($data = [])
    {
        $query = "INSERT INTO messages_to_forward (update_id, message_id, from_chat_id, json, is_forwarded) VALUES ('" . $data['update_id'] . "', '" . $data['message_id'] . "', '" . $data['from_chat_id'] . "', '" . $data['json'] . "', '" . $data['is_forwarded'] . "')";

        return $this->db->exec($query);
    }

    public function updateIsForwarded($message_id, $is_forwarded)
    {
        $query = "UPDATE messages_to_forward set is_forwarded = '" . $is_forwarded . "' WHERE message_id = '" . $message_id . "'";
        return $this->db->exec($query);
    }

    public function isMessageInTable($message_id)
    {
        $query = "SELECT id FROM messages_to_forward WHERE message_id = '" . $message_id . "'";
        $result = $this->db->query($query);
        return ($result->fetchArray()['id']) ?? null;
    }

    public function getUnfowarded()
    {
        $query = "SELECT message_id FROM messages_to_forward WHERE is_forwarded = 0";
        //$query = "SELECT message_id FROM messages_to_forward";
        $result = $this->db->query($query);
        $export = [];
        while ($row = $result->fetchArray()) {
            $export[] = $row['message_id'];
        }
        return $export;
    }
}
