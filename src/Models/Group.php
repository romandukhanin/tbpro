<?php

namespace App\Models;

class Group
{

    public $db;

    public function __construct()
    {
        $base = '../db/sqlitepizzabot.db';
        $this->db = new \SQLite3($base);
        $query = 'CREATE TABLE IF NOT EXISTS groups (id INTEGER PRIMARY KEY, chat_id STRING UNIQUE, title STRING, username STRING, date STRING, status STRING)';
        $this->db->exec($query);
    }

    public function getGroups()
    {
        $export = [];
        //$query = "SELECT title, username FROM groups WHERE status = 'member'";
        $query = "SELECT title, chat_id FROM groups WHERE status = 'member'";
        $result = $this->db->query($query);
        while ($row = $result->fetchArray()) {
            //$export[$row['title']] = $row['username'];
            $export[$row['title']] = $row['chat_id'];
        }
        return $export;
    }
    public function insertGroupToTable($data = [])
    {
        $query = "INSERT INTO groups (chat_id, title, username, date, status) VALUES ('" . $data['chat_id'] . "', '" . $data['title'] . "', '" . $data['username'] . "', '" . $data['date'] . "', '" . $data['status'] . "')";

        return $this->db->exec($query);
    }

    public function isUsernameInTable($username)
    {
        $query = "SELECT id FROM groups WHERE username = '" . $username . "'";
        $result = $this->db->query($query);
        return ($result->fetchArray()['id']) ?? null;
    }

    public function isChatIdInTable($chat_id)
    {
        $query = "SELECT id FROM groups WHERE chat_id = '" . $chat_id . "'";
        $result = $this->db->query($query);
        return ($result->fetchArray()['id']) ?? null;
    }


    public function updateStatus($chat_id, $status)
    {
        $query = "UPDATE groups set status = '" . $status . "' WHERE chat_id = '" . $chat_id . "'";
        return $this->db->exec($query);
    }
}
