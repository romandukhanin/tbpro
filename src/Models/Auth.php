<?php

namespace App\Models;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class Auth
{
    public $pass_token;

    public function __construct()
    {
        $this->pass_token = getenv('PASS_TOKEN');
    }

    public function check()
    {
        $request = Request::createFromGlobals();
        if (trim($request->headers->get('pass-token')) == $this->pass_token) {
            return true;
        }
        if (trim($request->query->get('pass_token')) == $this->pass_token) {
            return true;
        }
        return false;
    }

    public function responseNoAuth()
    {
        $message = [
            'result' => 'success',
            'message' => 'noauth',
        ];
        $this->sendResponse($message);
    }

    public function responseAuth()
    {
        $message = [
            'result' => 'success',
            'message' => 'auth',
        ];
        $this->sendResponse($message);
    }

    private function sendResponse($message)
    {
        $response = new Response(
            json_encode($message, JSON_UNESCAPED_UNICODE),
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
        $response->send();
    }
}
