<?php

namespace App\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TelegramRdk
{

    public $client;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }


    public function main()
    {
        /* $sendMessageOptions = [
            'chat_id' => '-1001120962653',
            'text' => 'Guzzle-muzzle11', 'token' => getenv('TELEGRAM_TOKEN')
        ];
        echo $this->sendMessage($sendMessageOptions); */

        /* $forwardMessageOptions = [
            'chat_id' => '-1001213500760',
            'from_chat_id' => getenv('SOURCE_GROUP_ID'),
            'message_id' => '1620',
            'token' => getenv('TELEGRAM_TOKEN')
        ];
        echo $this->forwardMessage($forwardMessageOptions); */

        $getUpdatesOptions = [
            //'allowed_updates' => ['message'],
            'token' => getenv('TELEGRAM_TOKEN')
        ];

        $result = $this->getUpdates($getUpdatesOptions);
        /* $response = new Response(
            $result,
            Response::HTTP_OK,
            ['content-type' => 'application/json']
        );
        $response->send(); */
        echo $result;
    }

    public function sendMessage($options = [])
    {

        $url = 'https://api.telegram.org/bot' .
            $options['token']
            . '/sendMessage';

        $res = $this->client->request(
            'POST',
            $url,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'chat_id' => $options['chat_id'],
                    'text' => $options['text']
                ]
            ]
        );
        //echo $res->getStatusCode();
        //echo $res->getHeader('content-type')[0];
        return $res->getBody();
    }

    public function forwardMessage($options = [])
    {

        $url = 'https://api.telegram.org/bot' .
            $options['token']
            . '/forwardMessage';

        $res = $this->client->request(
            'POST',
            $url,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    'chat_id' => $options['chat_id'],
                    'from_chat_id' => $options['from_chat_id'],
                    'message_id' => $options['message_id']
                ]
            ]
        );
        return $res->getBody();
    }

    public function getUpdates($options = [])
    {

        $url = 'https://api.telegram.org/bot' .
            $options['token']
            . '/getUpdates';

        $res = $this->client->request(
            'POST',
            $url,
            [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'json' => [
                    //'allowed_updates' => $options['allowed_updates'],
                ]
            ]
        );
        return $res->getBody();
    }
}
